import { Component } from '@angular/core';
import { User} from './user';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { Input } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { TemplateRef } from '@angular/core';
import { OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',

  styleUrls: ['./app.component.css']
})

export class AppComponent {


  topics = ['Angular2','Angula4','Angular5','Angular6'];
  topicHasError = true;

  userModel = new User ('', '', null,'default','morning',true);
  validateTopic(value) {
    if (value.topic ==='default') {
      this.topicHasError = true;
    } else {
      this.topicHasError = false;
    }

  }
  onSubmit(){
    console.log(this.userModel);
  }

    modalRef: BsModalRef;
    constructor(private modalService: BsModalService, private _http: HttpClient)  {}

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }



  _posts = [];
  _post = {};
  @ViewChild('modal') _myModal: any;

  ngOnInit() {
    return this._http.get<any>("http://jsonplaceholder.typicode.com/posts").subscribe(
      res => {
        this._posts = res;
      }
    );
  }
  showPost(postId: number) {
    this._http.get<any>(`http://jsonplaceholder.typicode.com/posts/${postId}`).subscribe(
      res => {
        this._post = res;
        this._myModal.nativeElement.style.display = 'block';
      }
    )
  }
}
